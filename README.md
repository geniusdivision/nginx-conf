Create a Digital Ocean server following the naming convention.

Server Size: 2GB RAM, 2 CPUs
Region: London
PHP Version: Latest Available
Post-Provision Recipe: Genius Division
Database: MySQL Latest Available
Database Name: Leave as default

Assign a floating IP in DO. Update the metadata in Forge.

# AWS CLI
`sudo apt install awscli`

`aws configure`

Create IAM user in Backups Group

Create Bucket, copying settings from com.geniusdivision.akita

com.geniusdivision.servername

Create Policy for writing to this bucket

```
{
   "Version": "2012-10-17",
   "Statement": [
     {
       "Effect": "Allow",
       "Action": ["s3:ListBucket"],
       "Resource": ["arn:aws:s3:::com.geniusdivision.servername"]
     },
     {
       "Effect": "Allow",
       "Action": [
         "s3:PutObject",
         "s3:GetObject"
       ],
       "Resource": ["arn:aws:s3:::com.geniusdivision.servername/*"]
     }
   ]
 }
```

# Configure Backup (WordPress)
`mkdir -p .scripts && wget -O .scripts/backup.sh https://bitbucket.org/geniusdivision/nginx-conf/raw/master/backup.sh`

Change `FORGEDBPASSWORD` for default forge password.

# Configure Backup (Laravel)
`mkdir -p .scripts && wget -O .scripts/backup.sh https://bitbucket.org/geniusdivision/nginx-conf/raw/master/backup_laravel.sh`

Change `FORGEDBPASSWORD` for default forge password.

# Configure Updates (WordPress)
`mkdir -p .scripts && wget -O .scripts/wp-update.sh https://bitbucket.org/geniusdivision/nginx-conf/raw/master/wp-update.sh`

# Cron Jobs

## Nightly Backups

Use the nightly option in Forge.

`0 0 * * *`

`sh /home/forge/.scripts/backup.sh`

## WordPress updates

We update WP sites at 07:30 every Tuesday.

`30 7 * * 2`

`sh /home/forge/.scripts/wp-update.sh`

# Configure failtoban (WordPress)
`cd /etc/fail2ban`

`sudo cp jail.conf jail.local`
```
sudo wget -O /etc/fail2ban/filter.d/wordpress-hard.conf https://bitbucket.org/geniusdivision/nginx-conf/raw/master/wordpress-hard.conf  
sudo wget -O /etc/fail2ban/filter.d/wordpress-soft.conf https://bitbucket.org/geniusdivision/nginx-conf/raw/master/wordpress-soft.conf
sudo wget -O /etc/fail2ban/filter.d/wordpress-xmlrpc.conf https://bitbucket.org/geniusdivision/nginx-conf/raw/master/wordpress-xmlrpc.conf
sudo wget -O /etc/fail2ban/filter.d/nginx-badbots.conf https://bitbucket.org/geniusdivision/nginx-conf/raw/master/nginx-badbots.conf
sudo wget -O /etc/fail2ban/filter.d/wordpress-badip.conf https://bitbucket.org/geniusdivision/nginx-conf/raw/master/wordpress-badip.conf
```

Add the following to /etc/fail2ban/jail.local


```
[wordpress-hard]
enabled = true
filter = wordpress-hard
logpath = /var/log/auth.log
maxretry = 5
port = http,https
bantime = 14400
# 14400 = 4 hours

[wordpress-soft]
enabled = true
filter = wordpress-soft
logpath = /var/log/auth.log
maxretry = 3
port = http,https
bantime = 1800

[wordpress-xmlrpc]
enabled = true
filter = wordpress-xmlrpc
logpath = /var/log/auth.log
maxretry = 1
port = http,https
bantime = 86400
#24 hours

[nginx-badbots]
enabled  = true
port     = http,https
filter   = nginx-badbots
logpath  = /var/log/nginx/*.log
maxretry = 2

```

`sudo fail2ban-client reload`