#! /bin/bash

DAY=$(date "+%d")
MONTH=$(date "+%m")
YEAR=$(date "+%Y")
HOST=$(hostname)
HOSTNAME=$(echo "$HOST" | tr '[:upper:]' '[:lower:]')
BUCKETNAME="com.geniusdivision.$HOSTNAME"


BACKUP_DIR="/home/forge/.backup"
MYSQL_USER="forge"
MYSQL_PASSWORD="FORGEDBPASSWORD"

mkdir -p "$BACKUP_DIR"

mysql -u$MYSQL_USER -p$MYSQL_PASSWORD -N -e 'show databases' | while read dbname; do mysqldump -u$MYSQL_USER -p$MYSQL_PASSWORD --complete-insert "$dbname" | gzip > "$BACKUP_DIR/$YEAR-$MONTH-$DAY-$dbname".sql.gz; done;

aws s3 cp --recursive /home/forge/.backup/ "s3://$BUCKETNAME/_backups/_sql/"

rm -rf ./.backup

for d in /home/forge/*/ ; do
    ( cd $d/storage/app && aws s3 sync public "s3://$BUCKETNAME/_backups/$(basename $d)/storage/app/public")
done