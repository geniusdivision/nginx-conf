#! /bin/bash

for d in /home/forge/*/ ; do
	( cd $d/web && wp core update)
	( cd $d/web && wp plugin update --all)
done